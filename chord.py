#!/usr/bin/env python2.7

import math
import matplotlib.pyplot as plt
import random

def adj_x_y_text(x, y):
    offset = 0.08
    (text_x, text_y) = (x + offset * x, y + offset * y)
    return (text_x, text_y)

def main(seed):
    m = 6
    max_n = 2 ** m
    nr_nodes = 20

    random.seed(seed)
    nodes = random.sample(range(max_n), nr_nodes)
    nodes = sorted(nodes)
    # print(nodes)
    # Example configuration from lecture slide 35
    # nodes = [1, 8, 14, 21, 32, 38, 42, 48, 51, 56]

    plt.figure(figsize=(6.0, 6.0))
    circle = plt.Circle((0, 0), 1.0, lw=2.0, ls='solid', facecolor='white', edgecolor='black')
    plt.gcf().gca().add_artist(circle)
    ax = plt.gca()
    
    ax.set_xlim((-1, 1))
    ax.set_ylim((-1, 1))
    plt.axis('off')

    for node in nodes:
        # 0 <= t <= 2 * pi
        # + math.pi / 2 to make node 0 start at (0, 1) instead of (1, 0)
        t = 2 * math.pi - node * (2 * math.pi / float(max_n)) + math.pi/2.
        x = math.cos(t)
        y = math.sin(t)
        plt.plot(x, y, marker='s', markersize=10, clip_on=False, color='lightblue')
        text_x, text_y = adj_x_y_text(x, y)
        # plt.text(text_x, text_y, '%.2f, %.2f'%(x, y), ha='center', va='center')
        plt.text(text_x, text_y, node, ha='center', va='center')

    # Plot ring start marker
    plt.plot([0, 0], [0.98, 1.02], clip_on=False, lw=1.5)

    plt.title('m=%d' % (m), x=0.9, y=1.)
    plt.savefig('chord-seed=%d.pdf' %(seed) )

    print_finger_tables(nodes, m)

def lookup_key(key, nodes):
    for node in nodes:
        if key <= node:
            return node
    return nodes[0]

def finger_table(n, nodes, m):
    fingers = []
    for i in range(m):
        key = n + 2 ** i
        key = key % (2 ** m)
        fingers.append(lookup_key(key, nodes))
                       
    assert(len(fingers) == m)
    return fingers

def print_finger_tables(nodes, m):
    max_n = 2 ** m
    for node in nodes:
        fingers = finger_table(node, nodes, m)
        print(node, fingers)

def print_lookup_path(key, n, nodes, m):
    pass
    
if __name__ == '__main__':
    main(100)
