#!/usr/bin/env python2.7

import math
import matplotlib.pyplot as plt
import numpy
import random
import time

m = 8 # key space size is 2^m
b = 2 # "base"
max_n = 2 ** m
L = 4 # neighbor table size

def adj_x_y_text(x, y):
    offset = 0.08
    (text_x, text_y) = (x + offset * x, y + offset * y)
    return (text_x, text_y)

def int_to_node(n):
    digits = m / b
    return (('%'+str(digits)+'s') % numpy.base_repr(n, 2 ** b)).replace(' ', '0')

def node_to_int(node):
    return int(node, 2 ** b)

def main(seed):
    nr_nodes = 20

    random.seed(seed)
    nodes = random.sample(range(max_n), nr_nodes)
    nodes = sorted(nodes)
    # print(nodes)
    # Example configuration from lecture slide 35
    # nodes = [1, 8, 14, 21, 32, 38, 42, 48, 51, 56]

    plt.figure(figsize=(6.0, 6.0))
    circle = plt.Circle((0, 0), 1.0, lw=2.0, ls='solid', facecolor='white', edgecolor='black')
    plt.gcf().gca().add_artist(circle)
    ax = plt.gca()
    
    ax.set_xlim((-1, 1))
    ax.set_ylim((-1, 1))
    plt.axis('off')

    for node in nodes:
        # 0 <= t <= 2 * pi
        # + math.pi / 2 to make node 0 start at (0, 1) instead of (1, 0)
        t = 2 * math.pi - node * (2 * math.pi / float(max_n)) + math.pi/2.
        x = math.cos(t)
        y = math.sin(t)
        plt.plot(x, y, marker='s', markersize=10, clip_on=False, color='lightblue')
        text_x, text_y = adj_x_y_text(x, y)

        ha = 'left'
        if x < 0: ha = 'right'
        plt.text(text_x, text_y, int_to_node(node), ha=ha, va='center')

    # Plot ring start marker
    plt.plot([0, 0], [0.98, 1.02], clip_on=False, lw=1.5)
    
    plt.title('m=%d, b=%d, seed=%d' % (m, b, seed), x=0.9, y=1.04)
    plt.savefig('pastry.pdf')

    print_routing_tables(nodes)
    # print_lookup_path(54, nodes, m)

def print_routing_table(table):
    digits = m / b
    for row in table:
        # for col in row:
        #    fmt = "%"+str(digits)+"s"
        #    print fmt % col,
        #print("")
        fmt = '%'+str(digits)+'s\t'
        fmt = fmt * len(row)
        print fmt % tuple(row)
        

# Defines and arbitrary proximity metric to break ties for nodes with
# common prefixes.
def proximity(n1, n2):
    pass

def lookup_key(key, nodes):
    for node in nodes:
        if key <= node:
            return node
    return nodes[0]

def routing_table(n, nodes):
    table = []
    nid = int_to_node(n)
    nids = [int_to_node(x) for x in nodes]
    
    for prefix_length in range(int(m / b)):
        row = []
        table.append(row)
        for digit in range(2 ** b):
            if str(digit) == nid[prefix_length]:
                row.append(str(digit))
                continue
            
            prefix = nid[0 : prefix_length]
            candidates = filter(lambda s : s.startswith(prefix+str(digit)), nids)
            if candidates:
                row.append(candidates[0])
            else:
                row.append("N/A")
                       
    return table

def print_routing_tables(nodes):
    max_n = 2 ** m
    for node in nodes:
        table = routing_table(node, nodes)
        print('Node\t%s' % (int_to_node(node)))
        print_routing_table(table)
        print ""

if __name__ == '__main__':
    main(100)
